# LUIS ANTONIO CHUCHON RIVERO  
![](https://scontent-mad1-1.xx.fbcdn.net/v/t1.0-9/14915399_1129886480414066_5692951047534255008_n.jpg?_nc_cat=103&_nc_ht=scontent-mad1-1.xx&oh=f0f6ede7f0e25191aa3e2747827397ed&oe=5CA906F5)  

*Me considero una persona trabajadora, amable, cordial,
responsable, puntual, organizada, sencilla, no tengo
problemas para relacionarme con mis compañeros de
trabajo y facilidad de aprendizaje.*

--------------------												---------------------
* Email 	                  									 	  24547226Q@gmail.com
	
* Movil 																		602058481

* Web													    [lchuchon](https://gitlab.com/lchuchon/curriculum-vitae/edit/master/cv_lchuchon.md)
--------------------												---------------------
>#### **HOBBIES**
 * Musica
 * Futbol
 * Peliculas y series

>#### **HABILIDAD**

* Comunicacion
* Empatia
* Trabajo en equipo
* Creatividad
* Flexibilidad


>#### **EDUCACIÓN**
 
* ##### **2017-2019 GRADO MEDIO** Instituto Poblenou;  
    Sistemas microinformáticos y redes, en curso.

* ##### **2015-2016 MARIA PARADO DE BELLIDO**;  
    ESO.


>#### **EXPERIENCIA LABORAL**

* ##### **2016 ATENCIÓN AL CLIENTE**  
    Encargado de la venta de componentes y diversos Software de computadora,   mantenimiento e instalación de Hardware y Software, configuración de PC a medida del cliente, empresa HORSAN SYSTEM.

* ##### **2013-2018 MANTENIMIENTO DE ORDENADORES**  
    Mantenimiento de instalación de sistemas operativos Windows y Linux, independiente.


>#### **IDIOMAS**

 | Castellano | Catalan | Ingles |
 | ---------- | ------- | ------ |
 | Nivel Materno | Nivel Basico | Nivel Basico |


>#### **INFORMATICA**

* Usuario de Windows y Linux.  
* Microsoft Office y OpenOffice.  
* Conocimiento de redes.  
* Diagnostico de conectividad PING Y traceroute.








